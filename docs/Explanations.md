**Parameters Required to be sent by drone**

Considering the the fact that drone sends data over of cellular modem.
And we need to only geolocation and speed of drone for our app,
Following paramaters will be sent by drone in every 8 seconds

1. Latitude: Float value, assumed unit in degree 
2. Longitude:Float value, assumed unit in degree
3. Altitude: Float value, assumed unit in meter
4. Timestamp: an Integer
5. Drone_Id: an alpha numeric string with maximum 8 characters

Drone_Id will be used for identifying drone and will also
Help in authentication (explained in protcol.md). So we wont
be needing additional api_key parameter

**Distance Calculation:**

Total distance travelled in an interval will have two components.
The XY component will be the distance between
last and current reported coordinates (latitude and longitude).   
The "Great Circle Distance" formula will be used to calculate XY component

Initially [Haversine](https://en.wikipedia.org/wiki/Great-circle_distance#Computational_formulas) method was used to calculate great circle distance. But there was difference of 5 meters in result and correct value , so 
[GeoPy](http://geopy.readthedocs.io/en/latest/#) library was used  


The Z component will be difference in current and last reported altitude  

Total distance = Square_root(Square(XY_Distance) + Square(Z_Distance))

**Speed Calculation:**  
Speed  = Total Distance travelled / Total time  
Total distance calculation is explained in previous section   
Total time is difference between current and last timestamp reported  


** Drone Not Moving alert**  
One requirement is to detect drones which are sending update,
But has not moved more than 1 meter in last 10 seconds. 
Drone Altitude is not considered while checking if drone satisfies not moving criteria.
Only great circle distance is considered  for this.
Not moving drones are highlighted using red color in ui.


** Drone Not Updating alert**
There can be cases when drone is not sending its location.
The system is designed in such a way that as soon as there
Is a location update submitted on server it will be sent to 
Any connected client.

At client side in every 15 seconds, codes check for drones for which no updates
Have been received, and it highlights them with yellow color.