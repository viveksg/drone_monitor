A drone swarm simulation can be started by executing   
"simulate.py".   
execution: **python3 simulate.py**
1. It first creates owners and prints out the list of ownerid and
   generated api key, which can be used at dashboard

2. it then registers drones
3. And then starts sending location update to server.
4. After every 1 minute it asks user to continue furthur or not.   

Important points 
1. Any current running instance of simulate.py should be closed.
   When server is stopped. This is because currently server
   doesnt stores data in persistant storage and use in memory
   solution for data. So if simulate.py is still running.
   It will send the data to server for drones which are not there.
   
2. If a client is subscribed to a room before execution of simulate.py   
   (this is possible for root client). The it may receive few drone data
   with all values 0, In case simulate.py only registers the drone, but
   not send any location data for the drone till that point.
   
   
   