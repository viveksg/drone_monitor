$(document).ready(function() {
    var drone_socket = io.connect("http://" + document.domain + ":" + location.port);
    var single_data = 2222;
    var bulk_data = 3333;
    var status_ok = 1000;
    var status_failed = 1001;
    var location_update_event = "location_update";
    var subscribe_event = "subscribe";
    var unsubscribe_event = "unsubscribe";
    var first_load = true;
    var location_data = [];
    var str_location_data = "drone_location_data";
    var str_drone_id = "drone_id";
    var str_latitude = "latitude";
    var str_longitude = "longitude";
    var str_altitude = "altitude";
    var str_speed = "speed";
    var str_last_updated = "last_updated";
    var str_is_moving = "is_moving";
    var str_is_updating = "is_updating";
    var request_status_event = "request_status";
    var current_request = 0;
    var subscribe_request = 10;
    var unsubscribe_request = 11;
    var has_subscribed = false;
    var data_keys = [str_drone_id, str_latitude, str_longitude, str_altitude, str_speed, str_last_updated];
    var full_data_keys = [str_drone_id, str_latitude, str_longitude, str_altitude, str_speed, str_last_updated, str_is_moving, str_is_updating];
    var drones = [];
    var data_table = "#data_tab";
    var api_key = "";
    var owner_id = "";
    var no_movement_class = "not_moving_alert";
    var no_update_class = "no_update_alert";
    var subscribed = true
    var allowed_no_update_interval = 15;
    var background_color = "#3c4556";
    var backgroud_color_property  ="background-color";
    setInterval(alert_check_callback, allowed_no_update_interval * 1000);
    $('body').css(backgroud_color_property,background_color);
    drone_socket.on(location_update_event, function(msg) {
        set_data(msg);
    });

    drone_socket.on(request_status_event, function(msg) {
        console.log(msg);
        if (msg.status = status_ok) {
            if (current_request == subscribe_request) {
                alert("subscribed");
                has_subscribed = true;
            } else {
                alert("unsubscribed");
                reset_state();
            }
        }
        current_request = 0;
    });
    $("#sub").click(function() {
        if(data_invalid())
            return;
        current_request = subscribe_request;
        subscribe();
    });

    $("#un_sub").click(function() {
        if(data_invalid())
           return;
        current_request = unsubscribe_request;
        unsubscribe();
    });

    function data_invalid(){
       api_key = $("#api_input").val();
       owner_id = $("#owner_input").val();
       if (api_key.length == 0 || owner_id.length == 0) {
            alert("ownerId or api_key empty");
            return true;
        }
        return false;
    }

    function subscribe() {
        send_socket_event(subscribe_event);
    }

    function unsubscribe() {
        send_socket_event(unsubscribe_event);
    }

    function reset_state() {
        drones = [];
        current_request = 0;
        has_subscribed = false;
        api_key = "";
        owner_input = "";
        reset_ui();
    }

    function reset_ui() {
        $(data_table + " tbody").empty();
    }

    function send_socket_event(event_type) {
        drone_socket.emit(event_type, {
            api_key: api_key,
            owner_id: owner_id
        });
    }

    function set_data(loc_data) {
        var i = 0;
        if (loc_data.message_type == single_data)
            handle_drone_update(loc_data.data);
        else
            for (i = 0; i < loc_data.data.length; i++) {
                handle_drone_update(loc_data.data[i]);
            }
    }

    function handle_drone_update(data) {
        var prepend = true;
        var drone_id = data.drone_id;
        var temp_data = [];
        temp_data[str_drone_id] = data.drone_id;
        temp_data[str_latitude] = data.latitude;
        temp_data[str_longitude] = data.longitude;
        temp_data[str_altitude] = data.altitude;
        temp_data[str_last_updated] = data.last_updated;
        temp_data[str_speed] = data.speed;
        temp_data[str_is_moving] = data.is_moving;
        temp_data[str_is_updating] = data.is_updating;
        if (drones[drone_id]) {
            prepend = false;
        }

        drones[drone_id] = temp_data;
        var is_drone_moving = temp_data[str_is_moving];
        var row_data = get_row_html(temp_data, prepend == false);
        if (prepend) {
            add_row(row_data, is_drone_moving);
        } else
            update_row(drone_id, row_data);
        $("#" + drone_id).removeClass(no_update_class);
        $("#" + drone_id).removeClass(no_movement_class);
        if (temp_data[str_is_updating] == false)
            raise_alert(drone_id, no_update_class);
        if (temp_data[str_is_moving] == false)
            raise_alert(drone_id, no_movement_class);
        console.log(row_data);
    }


    function get_row_html(data, only_inner_html_required) {
        var i = 0;
        var inner_html = "";
        var full_html = "";
        var row_start = "<tr id ='" + data[str_drone_id] + "'>";
        var row_end = "</tr>";
        for (i = 0; i < data_keys.length; i++)
            inner_html += "<td>" + ((data_keys[i]==str_last_updated)?get_local_timeString(data[data_keys[i]]):data[data_keys[i]]) + "</td>";
        full_html = row_start + inner_html + row_end;
        return only_inner_html_required ? inner_html : full_html
    }

    function add_row(row_html, is_drone_moving) {
        $(data_table + " tbody").append(row_html);
    }

    function update_row(drone_id, row_html) {
        $("#" + drone_id).empty().append(row_html);
    }

    function raise_alert(drone_el, alert_class) {
        $("#" + drone_el).removeClass(alert_class);
        $("#" + drone_el).addClass(alert_class);

    }

    function alert_check_callback() {
        console.log("attempting alert..");
        if (!has_subscribed)
            return;
        var drone;
        for (drone in drones)
            check_for_alert(drone, drones[drone]);
    }

    function check_for_alert(drone_id, drone_data) {
        var curr_date = new Date();
        var last_updated = drone_data[str_last_updated];
        var current_time = parseInt((curr_date.getTime() / 1000));
        console.log(current_time + " " + last_updated + " " + allowed_no_update_interval + " " + (current_time - last_updated));
        if (current_time - last_updated > allowed_no_update_interval) {
            raise_alert(drone_id, no_update_class);
        }
    }

    function get_local_timeString(data_tstamp){
         if (data_tstamp< 100000)
            return data_tstamp;
         var date_val = new Date(data_tstamp*1000);
         return date_val.toLocaleString();
    }

});