import math
import calendar
import time
from geopy.distance import great_circle
from ..constants import Constants
import logging

class Drone:
    """Class describing drone attributes and calculations of distance and speed.
       For distance calculation Great Circle Distance formula is used
    """
    # coordinates are in float and in degrees unit and altitude is in float and in meters
    current_latitude = 0
    current_longitude = 0
    current_altitude = 0
    last_update_time_stamp = 0

    initial_latitude = 0
    initial_longitude = 0
    initial_altitude = 0
    first_update_timestamp = 0

    last_interval_seconds = 0

    # only considering great circle distance in recent_gc_distance
    recent_gc_distance = 0
    recent_vertical_distance = 0
    recent_total_distance = 0

    monitoring_started = False

    # in meter/second
    current_speed = 0
    last_update_has_invalid_coordinates = False
    def __init__(self, dr_id, own_id):
        self.drone_id = dr_id
        self.owner_id = own_id

    def handle_new_data(self, lat, lng, alt, tstamp):
        self.last_update_has_invalid_coordinates = Drone.is_location_invalid(lat,lng)
        if self.last_update_has_invalid_coordinates:
            return False
        tstamp = Drone.check_timestamp(tstamp)
        if self.monitoring_started:
            self.calculate_distance(lat, lng, alt)
            self.calculate_speed(tstamp)
            self.last_interval_seconds = tstamp - self.last_update_time_stamp
        self.update_location_data(lat, lng, alt, tstamp)
        if not self.monitoring_started:
            self.monitoring_started = True
        return True

    @staticmethod
    def is_location_invalid(lat, lng):
        if math.fabs(lat) > 90.0 or math.fabs(lng) > 180.0:
            logging.warning(Constants.INVALID_COORDINATE_WARNING)
            #raise ValueError("Invalid coordinates exception")
            return True
        return False

    @staticmethod
    def check_timestamp(tstamp):
        if tstamp < 0:
            return calendar.timegm(time.gmtime())
        return tstamp

    def calculate_distance(self, lat, lng, alt):
        self.recent_gc_distance = great_circle((self.current_latitude, self.current_longitude),
                                               (lat, lng)).meters
        self.recent_vertical_distance = math.fabs(alt - self.current_altitude)
        gc_square = math.pow(self.recent_gc_distance, 2)
        vertical_square = math.pow(self.recent_vertical_distance, 2)
        self.recent_total_distance = math.sqrt(gc_square + vertical_square)

    """
    @staticmethod
    def get_great_circle_distance(old_lat, old_lng, new_lat, new_lng):
        old_lat_radians = math.radians(old_lat)
        old_lng_radians = math.radians(old_lng)
        new_lat_radians = math.radians(new_lat)
        new_lng_radinas = math.radians(new_lng)
        delta_lat = new_lat_radians - old_lat_radians
        delta_lng = new_lng_radinas - old_lng_radians
        sine_sq_lat = math.pow(math.sin(delta_lat*0.5), 2)
        sine_sq_lng = math.pow(math.sin(delta_lng*0.5), 2)
        cos_lat_product = new_lat_radians * old_lat_radians
        central_angle = 2*math.asin(math.sqrt(sine_sq_lat+cos_lat_product*sine_sq_lng))
        return Constants.EARTH_RADIUS * central_angle * 1000
    """

    def calculate_speed(self, tstamp):
        if tstamp > self.last_update_time_stamp:
            self.current_speed = self.recent_total_distance / (tstamp - self.last_update_time_stamp)
        else:
            logging.warning(Constants.SPEED_CALCULATION_WARNING)

    def update_location_data(self, lat, lng, alt, tstamp):
        if not self.monitoring_started:
            self.initial_latitude = lat
            self.initial_latitude = lng
            self.initial_altitude = alt
            self.first_update_timestamp = tstamp

        self.current_latitude = lat
        self.current_longitude = lng
        self.current_altitude = alt
        self.last_update_time_stamp = tstamp

    def is_moving(self):
        if self.recent_gc_distance <= Constants.MIN_DISTANCE and self.last_interval_seconds >= 10:
            return False
        return True

    def is_updating(self):
        if calendar.timegm(time.gmtime()) - self.last_update_time_stamp > 2 * Constants.DRONE_UPDATE_FREQ:
            return False
        return True

    def get_data(self):
        values = dict()
        values["drone_id"] = self.drone_id
        values["latitude"] = self.current_latitude
        values["longitude"] = self.current_longitude
        values["altitude"] = round(self.current_altitude,Constants.DEFAULT_ROUND_OPTION)
        values["speed"] = round(self.current_speed,Constants.SPEED_ROUND_OPTION)
        values["last_updated"] = self.last_update_time_stamp
        values["is_moving"] = self.is_moving()
        values["is_updating"] = self.is_updating()
        return values
