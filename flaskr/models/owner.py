class Owner:
    drones = []

    def __init__(self, oid, api):
        self.owner_id = oid
        self.api_key = api

    def add_new_drone(self, drone_id):
        self.drones.append(drone_id)

    def remove_drone(self, drone_id):
        self.drones.remove(drone_id)

    def get_drone_list(self):
        return self.drones

