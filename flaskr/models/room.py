import calendar
import time
class Room:
    connected_clients = 0
    sending_messages = False
    last_sent_update_timestamp = 0

    def __init__(self, rid):
        self.room_id = rid

    def increment_connected_clients(self):
        self.connected_clients = self.connected_clients + 1

    def decrement_connected_clients(self):
        if self.connected_clients > 0:
            self.connected_clients = self.connected_clients - 1

    def contains_active_clients(self):
        return self.connected_clients > 0

    """
       methods below kept of experimental feature,
       No need to pay attention below for now
    """

    def bulk_messaging_started(self):
        self.sending_messages = True

    def bulk_messaging_stopped(self):
        self.sending_messages = False
        self.last_sent_update_timestamp = calendar.timegm(time.gmtime())

    def is_bulk_messaging(self):
        return self.sending_messages
