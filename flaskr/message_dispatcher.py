from .constants import Constants
import json


class MessageDispatcher():
    drone_socket = None
    drone_manager = None
    message_queue = None

    def __init__(self, socket, dmgr, msg_q):
        self.drone_socket = socket
        self.drone_manager = dmgr
        self.message_queue = msg_q

    def message_dispatch(self):
        keep_running = True
        while (keep_running):
            if self.message_queue.empty():
                self.drone_socket.sleep(Constants.MESSAGE_THREAD_SLEEP_DURATION)
                continue
            message_type, drone_id, owner_id, private_room_id = self.message_queue.get()
            if message_type == Constants.MESSAGE_SINGLE_DRONE:
                rooms = []
                owner = self.drone_manager.get_owner_id_from_drone(drone_id)
                rooms.append(owner)
                if not self.drone_manager.is_root(owner):
                    rooms.append(self.drone_manager.get_root_room_id())
                room_size = len(rooms)
                for i in range(room_size):
                    if self.drone_manager.should_send_location_data(rooms[i]):
                        data = self.drone_manager.get_drone_data(drone_id)
                        if data is not None:
                            json_data = json.loads(json.dumps(data))
                            self.drone_socket.emit(Constants.LOCATION_UPDATE_EVENT, json_data, room=rooms[i])


            elif message_type == Constants.MESSAGE_ALL_DRONE_DATA:
                final_data = dict()
                final_data[Constants.MESSAGE_TYPE] = Constants.MESSAGE_ALL_DRONE_DATA
                json_data = json.loads(json.dumps(self.drone_manager.get_drone_data_for_owner(owner_id)))
                final_data[Constants.DATA] = json_data
                room_id = owner_id if private_room_id is None else private_room_id
                self.drone_socket.emit(Constants.LOCATION_UPDATE_EVENT, final_data, room=room_id)

            elif message_type == Constants.STOP_MESSAGE_DISPATCH:
                keep_running = False
            self.message_queue.task_done()

    def shutdown(self):
        self.message_queue.put((Constants.STOP_MESSAGE_DISPATCH, None, None, None))
