from queue import Queue
from flask import Flask, jsonify, request, render_template
from flask_socketio import SocketIO, join_room, leave_room, send, emit
from .constants import Constants
from .controller import DroneManager
from .message_dispatcher import MessageDispatcher

from threading import Lock

drone_monitor = Flask("__name__", template_folder='flaskr/templates')
drone_socket = SocketIO(drone_monitor)
drone_manager = DroneManager()
message_queue = Queue()
dispatcher = MessageDispatcher(drone_socket, drone_manager, message_queue)
thread_lock = Lock()
with thread_lock:
    message_worker = drone_socket.start_background_task(target=dispatcher.message_dispatch)


@drone_monitor.route(Constants.ROUTE_TEST)
def test_request():
    response = {Constants.API_STATUS: Constants.STATUS_API_WORKING}
    return jsonify(response)


@drone_monitor.route(Constants.ROUTE_TESTCONFIG)
def test_config_request():
    if drone_monitor.testing:
        drone_manager.create_test_config()
        return jsonify({Constants.STATUS: Constants.MESSAGE_TEST_ENVIRONMENT_ACTIVE})


@drone_monitor.route(Constants.ROUTE_DASHBOARD)
def dashboard_request():
    return render_template(Constants.TEMPLATE_DASHBOARD)


@drone_monitor.route(Constants.ROUTE_LOCATION, methods=["POST"])
def location_submit_request():
    drone_id = request.form[Constants.PARAMS_DRONE_ID]
    latitude = float(request.form[Constants.PARAMS_LAT])
    longitude = float(request.form[Constants.PARAMS_LNG])
    altitude = float(request.form[Constants.PARAMS_ALT])
    timestamp = int(request.form[Constants.PARAMS_TSTAMP])
    status = drone_manager.handle_new_location(drone_id, latitude, longitude, altitude, timestamp)
    message_queue.put((Constants.MESSAGE_SINGLE_DRONE, drone_id, None, None))
    result = {Constants.STATUS: status}
    return jsonify(result)


@drone_monitor.route(Constants.ROUTE_ADD_DRONE, methods=["POST"])
def add_new_drone_request():
    drone_id = request.form[Constants.PARAMS_DRONE_ID]
    owner_id = request.form[Constants.PARAMS_OWNER_ID]
    api_key = request.form[Constants.PARAMS_API_KEY]
    status = drone_manager.handle_add_new_drone(drone_id, owner_id, api_key)
    result = {"status": status}
    return jsonify(result)


@drone_monitor.route(Constants.ROUTE_ADD_OWNER, methods=["POST"])
def add_new_owner_request():
    owner_id = request.form[Constants.PARAMS_OWNER_ID]
    api_key = request.form[Constants.PARAMS_API_KEY]
    result = drone_manager.handle_create_new_owner(owner_id, api_key)
    return jsonify(result)


@drone_socket.on(Constants.SUBSCRIBE_EVENT)
def handle_subscribe_request(data):
    owner_id = data[Constants.PARAMS_OWNER_ID]
    api_key = data[Constants.PARAMS_API_KEY]
    if drone_manager.handle_subscribe_requests(owner_id, api_key,
                                               Constants.SUBSCRIBE_REQUEST) == Constants.OPERATION_OK:
        data = {Constants.STATUS: Constants.OPERATION_OK}
        emit(Constants.REQUEST_STATUS_EVENT, data, room=request.sid)
        join_room(owner_id)
        message_queue.put((Constants.MESSAGE_ALL_DRONE_DATA, None, owner_id, request.sid))
    else:
        data = {Constants.STATUS: Constants.OPERATION_FAILED}
        emit(Constants.REQUEST_STATUS_EVENT, data, room=request.sid)


@drone_socket.on(Constants.UNSUBSCRIBE_EVENT)
def handle_unsubscribe_request(data):
    owner_id = data[Constants.PARAMS_OWNER_ID]
    api_key = data[Constants.PARAMS_API_KEY]
    if drone_manager.handle_subscribe_requests(owner_id, api_key,
                                               Constants.UNSUBSCRIBE_REQUEST) == Constants.OPERATION_OK:
        data = {Constants.STATUS: Constants.OPERATION_OK}
        emit(Constants.REQUEST_STATUS_EVENT, data, room=request.sid)
        leave_room(owner_id)
    else:
        data = {Constants.STATUS: Constants.OPERATION_FAILED}
        emit(Constants.REQUEST_STATUS_EVENT, data, room=request.sid)


def stop_dispatcher():
    dispatcher.shutdown()


# experimental feature
@drone_socket.on(Constants.CHECK_DRONES_WITH_NO_UPDATES)
def handle_check_for_alerts(data):
    pass
