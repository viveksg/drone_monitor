from .models.drone import Drone
from .models.api_key import Api_Key
from .models.owner import Owner
from .models.room import Room
from .constants import Constants


class DataManager:
    """
    This class is an in-memory database solution required for this task
    """

    __apis = None
    __owners = None
    __drones = None
    __rooms = None

    def __init__(self):
        self.__apis = dict()
        self.__owners = dict()
        self.__drones = dict()
        self.__rooms = dict()

    def add_new_drone(self, drone_id, owner_id):
        drone = Drone(drone_id, owner_id)
        self.__drones[drone_id] = drone

    def drone_exists(self, drone_id):
        return drone_id in self.__drones

    def attach_drone_to_owner(self, drone_id, owner_id):
        owner = self.__owners[owner_id]
        owner.add_new_drone(drone_id)

    def update_drone_data(self, drone_id, lat, lng, alt, tstamp):
        drone = self.__drones[drone_id]
        return drone.handle_new_data(lat, lng, alt, tstamp)

    def add_new_api_key(self, owner_id, key):
        api_key = Api_Key(key, owner_id)
        self.__apis[key] = api_key

    def add_new_owner(self, owner_id, key):
        owner = Owner(owner_id, key)
        self.__owners[owner_id] = owner

    def owner_exists(self, owner_id):
        return owner_id in self.__owners

    def api_key_exists(self, api_key):
        return api_key in self.__apis

    def get_owner_for_api_key(self, api_key):
        if self.api_key_exists(api_key):
            api = self.__apis[api_key]
            return api.get_owner_id()
        return None

    def get_drone_owner(self, drone_id):
        drone = self.__drones[drone_id]
        return drone.owner_id if drone is not None else None

    def create_new_room(self, room_id):
        room = Room(room_id)
        self.__rooms[room_id] = room

    def room_exists(self, room_id):
        return room_id in self.__rooms

    def get_room(self, room_id):
        return self.__rooms[room_id] if self.room_exists(room_id) else None

    def get_drone(self, drone_id):
        return self.__drones[drone_id] if self.drone_exists(drone_id) else None

    def get_owner(self, owner_id):
        return self.__owners[owner_id] if self.owner_exists(owner_id) else None

    def get_all_drone_data(self):
        if len(self.__drones) == 0:
            return None
        data = []
        for drone in self.__drones:
            data.append(self.__drones[drone].get_data())
        return data

    def get_drone_data_in_list(self,drone_list):
        data = []
        for drone_id in drone_list:
            drone = self.get_drone(drone_id)
            if drone is not None:
                data.append(drone.get_data())
        return data

    def modify_room_client_count(self, room_id, increment):
        room = self.__rooms[room_id]
        if increment == 1:
            room.increment_connected_clients()
        elif increment == -1:
            room.decrement_connected_clients()
