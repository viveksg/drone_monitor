from .data_manager import DataManager
from .constants import Constants
from flask import jsonify
import random
import string


class DroneManager:
    """
     This class will perform required action on stored data,
     based on user Requests and handle server side operations.
    """
    data_mgr = None
    __root_user = None
    __root_api_key = None

    def __init__(self):
        self.data_mgr = DataManager()
        self.__root_api_key = "OhE7ORDPD6mvhnsjzQvjQ373q"
        self.__root_user = "root101"
        self.data_mgr.add_new_api_key(self.__root_user, self.__root_api_key)
        self.data_mgr.add_new_owner(self.__root_user, self.__root_api_key)

    def handle_new_location(self, drone_id, lat, lng, alt, tstamp):
        if self.data_mgr.drone_exists(drone_id):
            if self.data_mgr.update_drone_data(drone_id, lat, lng, alt, tstamp):
                return Constants.OPERATION_OK
        return Constants.OPERATION_FAILED

    def handle_create_new_owner(self, owner_id, supplied_key):
        if supplied_key == self.__root_api_key:  # new owner can only be created through root api key
            if self.data_mgr.owner_exists(owner_id):
                return {"status": Constants.DATA_ALREADY_PRESENT}
            new_api_key = self.random_id_generator()
            self.data_mgr.add_new_owner(owner_id, new_api_key)
            self.data_mgr.add_new_api_key(owner_id, new_api_key)
            return {Constants.PARAMS_API_KEY: new_api_key}
        return {"status": Constants.OPERATION_FAILED}

    def create_test_config(self):
        self.data_mgr.add_new_owner(Constants.TEST_OWNER_ID, Constants.TEST_API_KEY)
        self.data_mgr.add_new_api_key(Constants.TEST_OWNER_ID, Constants.TEST_API_KEY)
        self.data_mgr.add_new_drone(Constants.TEST_DRONE_ID, Constants.TEST_OWNER_ID)
        self.data_mgr.attach_drone_to_owner(Constants.TEST_DRONE_ID, Constants.TEST_OWNER_ID)

    def handle_add_new_drone(self, drone_id, supplied_owner, supplied_key):
        owner = self.data_mgr.get_owner_for_api_key(supplied_key)
        if owner is None or supplied_owner != owner:
            return Constants.OPERATION_FAILED
        if self.data_mgr.drone_exists(drone_id):
            return Constants.DATA_ALREADY_PRESENT
        self.data_mgr.add_new_drone(drone_id, owner)
        self.data_mgr.attach_drone_to_owner(drone_id, owner)
        return Constants.OPERATION_OK

    def handle_subscribe_requests(self, owner_id, api_key, request_type):
        if not self.verify_owner_credentials(owner_id, api_key):
            return Constants.OPERATION_FAILED
        # rooms are indexed using owner_id, variable room_id is used in following code just for clarity
        room_id = owner_id
        if request_type == Constants.SUBSCRIBE_REQUEST:
            self.handle_subscribe(room_id)
            return Constants.OPERATION_OK
        return self.handle_unsubscribe(room_id)

    def handle_subscribe(self, room_id):
        if not self.data_mgr.room_exists(room_id):
            self.data_mgr.create_new_room(room_id)
        self.data_mgr.modify_room_client_count(room_id, 1)

    def handle_unsubscribe(self, room_id):
        if self.data_mgr.room_exists(room_id):
            self.data_mgr.modify_room_client_count(room_id, -1)
            return Constants.OPERATION_OK
        return Constants.OPERATION_FAILED

    def verify_owner_credentials(self, owner_id, api_key):
        owner = self.data_mgr.get_owner_for_api_key(api_key)
        if owner is None or owner_id != owner:
            return False
        return True

    def should_send_location_data(self, room_id):
        room = self.data_mgr.get_room(room_id)
        return room is not None and room.contains_active_clients()

    def get_drone_data_for_owner(self, owner_id):
        if owner_id == self.__root_user:
            return self.data_mgr.get_all_drone_data()
        owner_object = self.data_mgr.get_owner(owner_id)
        drone_list = owner_object.get_drone_list()
        return self.data_mgr.get_drone_data_in_list(drone_list)

    def get_drone_data(self, drone_id):
        drone = self.data_mgr.get_drone(drone_id)
        if drone is not None:
            data = dict()
            data[Constants.MESSAGE_TYPE] = Constants.MESSAGE_SINGLE_DRONE
            data[Constants.DATA] = drone.get_data()
            return data
        return None

    def get_owner_id_from_drone(self, owner_id):
        return self.data_mgr.get_drone_owner(owner_id)

    def is_root(self, owner_id):
        return owner_id == self.__root_user

    def get_root_room_id(self):
        room_id = self.__root_user
        return room_id

    def random_id_generator(self):
        char_set = string.ascii_letters + string.digits
        while True:
            data = "".join(random.choice(char_set) for _ in range(Constants.API_KEY_SIZE))
            if not self.data_mgr.api_key_exists(data):
                break
        return data
