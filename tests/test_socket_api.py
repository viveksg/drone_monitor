import unittest
import json
import time
from flaskr.constants import Constants
from flaskr import request_handler
from flask_socketio import SocketIO, test_client


class TestSocketAPI(unittest.TestCase):
    socket_client = None
    app_client = None
    json_data = None

    def setUp(self):
        if self.socket_client is None or self.app_client is None:
            app = request_handler.drone_monitor
            app.testing = True
            self.app_client = request_handler.drone_monitor.test_client(app)
            self.socket_client = request_handler.drone_socket.test_client(app)
            data = dict()
            data[Constants.PARAMS_OWNER_ID] = Constants.TEST_OWNER_ID
            data[Constants.PARAMS_API_KEY] = Constants.TEST_API_KEY
            self.json_data = json.loads(json.dumps(data))

    def test_check_test_environment_active(self):
        response = self.app_client.get(Constants.ROUTE_TESTCONFIG)
        self.perform_assertion(response, Constants.STATUS, Constants.MESSAGE_TEST_ENVIRONMENT_ACTIVE)

    def test_subscribe(self):
        self.socket_client.emit(Constants.SUBSCRIBE_EVENT, self.json_data)
        response = self.socket_client.get_received()
        json_result = response[0]['args'][0]
        self.assertEqual(json_result[Constants.STATUS], Constants.OPERATION_OK)

    def test_unsubscribe(self):
        self.socket_client.emit(Constants.SUBSCRIBE_EVENT, self.json_data)
        self.socket_client.emit(Constants.UNSUBSCRIBE_EVENT, self.json_data)
        response = self.socket_client.get_received()
        json_result = response[1]['args'][0]
        self.assertEqual(json_result[Constants.STATUS], Constants.OPERATION_OK)

    def perform_assertion(self, response, param_name, expected_result):
        json_result = json.loads(response.data)
        self.assertEqual(json_result[param_name], expected_result)

    def tearDown(self):
        request_handler.dispatcher.shutdown()
