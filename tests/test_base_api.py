import unittest
import json
import time
from flaskr.constants import Constants
from flaskr import request_handler


class TestBaseAPI(unittest.TestCase):
    client = None
    __root_key = "OhE7ORDPD6mvhnsjzQvjQ373q"
    __root_owner_id = "root101"

    def setUp(self):
        if self.client is None:
            self.client = request_handler.drone_monitor.test_client()

    def test_server_running(self):
        response = self.client.get(Constants.ROUTE_TEST, content_type="json")
        self.perform_assertion(response, Constants.API_STATUS, 5555)

    def test_location_post(self):
        response = self.make_new_location_request("342", 23.1, 24.2, 233, int(time.time()))
        self.perform_assertion(response, Constants.STATUS, 1001)
        # send coordinates after adding drone
        add_drone_response = self.make_add_new_drone_request("342", self.__root_owner_id, self.__root_key)
        response = self.make_new_location_request("342", 23.1, 24.2, 233, int(time.time()))
        self.perform_assertion(response, Constants.STATUS, 1000)

    def test_add_new_user(self):
        response = self.make_add_new_owner_request("owner1", self.__root_key)
        json_result = json.loads(response.data)
        self.assertIsNotNone(json_result[Constants.PARAMS_API_KEY])

        # adding new user with invalid key
        response = self.make_add_new_owner_request("owner2", "fdsfsdfs")
        self.perform_assertion(response, Constants.STATUS, Constants.OPERATION_FAILED)

        # adding exiting user with invalid key, code should not send user already exists message
        response = self.make_add_new_owner_request("owner1", "fdsfsdfs")
        self.perform_assertion(response, Constants.STATUS, Constants.OPERATION_FAILED)

        # adding exiting user again with valid key
        response = self.make_add_new_owner_request("owner1", self.__root_key)
        self.perform_assertion(response, Constants.STATUS, Constants.DATA_ALREADY_PRESENT)

    def test_add_new_drone(self):
        response = self.make_add_new_drone_request("drone1", self.__root_owner_id, self.__root_key)
        self.perform_assertion(response, Constants.STATUS, Constants.OPERATION_OK)

        # adding same drone again
        response = self.make_add_new_drone_request("drone1", self.__root_owner_id, self.__root_key)
        self.perform_assertion(response, Constants.STATUS, Constants.DATA_ALREADY_PRESENT)

        # adding drone with wrong api_key
        response = self.make_add_new_drone_request("drone2", self.__root_owner_id, "dfsfsd")
        self.perform_assertion(response, Constants.STATUS, Constants.OPERATION_FAILED)

        # adding exiting drone with wrong api_key, no data info should be returned
        response = self.make_add_new_drone_request("drone1", self.__root_owner_id, "dasdsadsa")
        self.perform_assertion(response, Constants.STATUS, Constants.OPERATION_FAILED)

        # adding exiting drone with wrong owner_id, correct api_key
        response = self.make_add_new_drone_request("drone1", "dasdas", self.__root_key)
        self.perform_assertion(response, Constants.STATUS, Constants.OPERATION_FAILED)

        # adding new drone with wront owner_id, correct api_key
        response = self.make_add_new_drone_request("drone2", "frerwqr", self.__root_key)
        self.perform_assertion(response, Constants.STATUS, Constants.OPERATION_FAILED)

    def make_add_new_owner_request(self, owner_id, api_key):
        return self.client.post(Constants.ROUTE_ADD_OWNER, data={Constants.PARAMS_OWNER_ID: owner_id,
                                                                 Constants.PARAMS_API_KEY: api_key})

    def make_add_new_drone_request(self, drone_id, owner_id, api_key):
        return self.client.post(Constants.ROUTE_ADD_DRONE, data={Constants.PARAMS_DRONE_ID: drone_id,
                                                                 Constants.PARAMS_OWNER_ID: owner_id,
                                                                 Constants.PARAMS_API_KEY: api_key})

    def make_new_location_request(self, drone_id, lat, lng, alt, timestamp):
        return self.client.post(Constants.ROUTE_LOCATION, data={Constants.PARAMS_DRONE_ID: drone_id,
                                                                Constants.PARAMS_LAT: lat,
                                                                Constants.PARAMS_LNG: lng,
                                                                Constants.PARAMS_ALT: alt,
                                                                Constants.PARAMS_TSTAMP: timestamp})

    def perform_assertion(self, response, param_name, expected_result):
        json_result = json.loads(response.data)
        self.assertEqual(json_result[param_name], expected_result)
