import unittest
import math
from flaskr.models.drone import Drone
import time
import logging

class TestDroneModel(unittest.TestCase):
    drone = None
    duration_offset = 20
    def setUp(self):
        self.drone = Drone("x", "y")
        self.drone.handle_new_data(41.49008, -71.312796, 10, int(time.time()) - self.duration_offset)

    def test_distance_calculations(self):
        result = 864214.4943393626
        self.drone.handle_new_data(41.499498, -81.695391, 10, int(time.time()))
        self.assertEqual(self.drone.recent_gc_distance, result)
        self.assertEqual(self.drone.recent_total_distance, result)

    def test_distance_with_altitude(self):
        result = math.sqrt( math.pow(864214.4943393626,2) + 400)
        self.drone.handle_new_data(41.499498, -81.695391, 30, int(time.time()))
        self.assertEqual(self.drone.recent_total_distance, result)

    def test_distance_with_negative_altitude(self):
        result = math.sqrt(math.pow(864214.4943393626, 2) + 400)
        self.drone.handle_new_data(41.499498, -81.695391, -10, int(time.time()))
        self.assertEqual(self.drone.recent_total_distance, result)

    def test_invalid_coordinates(self):
        print("Testing invalid coordinates...")
        self.drone.handle_new_data(-21331, 324324, 32, int(time.time()))
        self.assertTrue(self.drone.last_update_has_invalid_coordinates)

    def test_speed_calculation(self):
        result = 864214.4943393626/self.duration_offset
        self.drone.handle_new_data(41.499498, -81.695391, 10, int(time.time()))
        self.assertEqual(self.drone.current_speed, result)

    def test_speed_calculation_with_positive_altitude(self):
        result = math.sqrt(math.pow(864214.4943393626, 2) + 400)/self.duration_offset
        self.drone.handle_new_data(41.499498, -81.695391, 30, int(time.time()))
        self.assertEqual(self.drone.current_speed, result)

    def test_speed_calculation_with_negative_altitude(self):
        result = math.sqrt(math.pow(864214.4943393626, 2) + 625)/self.duration_offset
        self.drone.handle_new_data(41.499498, -81.695391, -15, int(time.time()))
        self.assertEqual(self.drone.current_speed, result)

    def test_drone_not_moving(self):
        self.drone.handle_new_data(41.49008, -71.312796, 30, int(time.time()))
        self.assertFalse(self.drone.is_moving())

    def test_drone_not_updating(self):
        self.drone.handle_new_data(41.49008, -71.312796, 30, int(time.time()) - 17)
        self.assertFalse(self.drone.is_updating())

    def test_drone_updating(self):
        self.drone.handle_new_data(41.49008, -71.312796, 30, int(time.time()))
        self.assertTrue(self.drone.is_updating())
