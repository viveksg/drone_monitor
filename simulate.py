import random
import time
import json
import string
from urllib.request import Request, urlopen
from urllib.parse import urlencode
import calendar
from threading import Thread


class DroneSwarmSimulation:
    CONST_OWNER_ID = "owner_id"
    CONST_DRONE_ID = "drone_id"
    CONST_API_KEY = "api_key"
    CONST_LATITUDE = "lat"
    CONST_LONGITUE = "lng"
    CONST_ALTITUDE = "alt"
    CONST_TIMESTAMP = "tstamp"
    CONST_STATUS_OK = 1000
    CONST_STATUS_FAILED = 1001
    CONST_STATUS = "status"
    CONST_SWARM_DURATION = 60  # seconds
    __root_key = None
    __root_user = None
    base_url = "http://0.0.0.0:5000"
    add_owner_route = "/add_owner"
    add_drone_route = "/add_drone"
    location_route = "/location"
    owners_api_key = dict()
    owners = []
    drones = []
    not_moving_drones = [];
    not_moving_data = dict();
    def __init__(self):
        self.__root_key = "OhE7ORDPD6mvhnsjzQvjQ373q"
        self.__root_id = "root101"
        self.owners_api_key[self.__root_id] = self.__root_key

    def init_swarm(self):
        self.add_owners()
        self.add_drones()

    def add_owners(self):
        owner_count = 10 + random.randint(0, 10)
        add_owner_url = self.base_url + self.add_owner_route
        data = dict()
        data[self.CONST_API_KEY] = self.__root_key
        for i in range(owner_count):
            owner_id = "owner_" + self.get_random_string()
            data[self.CONST_OWNER_ID] = owner_id
            json_data = json.loads(self.send_request(add_owner_url, data))
            if self.CONST_STATUS in json_data:
                print(owner_id + " " + json_data[self.CONST_STATUS])
            else:
                self.owners_api_key[owner_id] = json_data[self.CONST_API_KEY]
                print(owner_id + " " + self.owners_api_key[owner_id])
                self.owners.append(owner_id)

    def add_drones(self):
        drone_count = 25 + random.randint(20, 30)
        add_drone_url = self.base_url + self.add_drone_route
        data = dict()
        owner_count = len(self.owners) - 1
        for i in range(drone_count):
            data.clear()
            drone_id = "drone_" + self.get_random_string()
            owner_id = self.owners[random.randint(0, owner_count)]
            api_key = self.owners_api_key[owner_id]
            data[self.CONST_OWNER_ID] = owner_id
            data[self.CONST_DRONE_ID] = drone_id
            data[self.CONST_API_KEY] = api_key
            json_data = json.loads(self.send_request(add_drone_url, data))
            if json_data[self.CONST_STATUS] == 1000:
                self.drones.append(drone_id)

    def start_swarm_simulation(self):
        drone_count = len(self.drones) - 1
        location_url = self.base_url + self.location_route
        data = dict()
        swarm_start_time = calendar.timegm(time.gmtime())
        while True:
            if int(time.time()) - swarm_start_time > self.CONST_SWARM_DURATION:
                self.not_moving_data.clear()
                self.not_moving_drones.clear()
                user_input = input("Press key C to continue, key S to stop: ")
                if user_input == 'S':
                    break
                swarm_start_time = calendar.timegm(time.gmtime())

            data.clear()
            drone_id = self.drones[random.randint(0, drone_count)]
            timestamp = calendar.timegm(time.gmtime())
            if drone_id not in self.not_moving_drones:
                  latitude = random.uniform(-90.0, 90.0)
                  longitude = random.uniform(-180.0, 180.0)
                  altitude = random.randint(10, 1000)
                  data[self.CONST_DRONE_ID] = drone_id
                  data[self.CONST_LATITUDE] = latitude
                  data[self.CONST_LONGITUE] = longitude
                  data[self.CONST_ALTITUDE] = altitude
                  data[self.CONST_TIMESTAMP] = timestamp
                  json.loads(self.send_request(location_url, data))
            else:
                  self.not_moving_data[drone_id][self.CONST_TIMESTAMP] = timestamp
                  json.loads(self.send_request(location_url, self.not_moving_data[drone_id]))
            if drone_id not in self.not_moving_drones and random.randint(43435, 9999999)%2==0:
                self.not_moving_drones.append(drone_id)
                self.not_moving_data[drone_id] = dict()
                self.not_moving_data[drone_id][self.CONST_DRONE_ID] = drone_id
                self.not_moving_data[drone_id][self.CONST_LATITUDE] = latitude
                self.not_moving_data[drone_id][self.CONST_LONGITUE] = longitude
                self.not_moving_data[drone_id][self.CONST_ALTITUDE] = altitude
                self.not_moving_data[drone_id][self.CONST_TIMESTAMP] = timestamp
            time.sleep(1)
           # guranteed not moving request per iteration
            if len(self.not_moving_drones) > 0:
                non_moving_drone_id = self.not_moving_drones[random.randint(0, len(self.not_moving_drones) - 1)]
                self.not_moving_data[non_moving_drone_id][self.CONST_TIMESTAMP] = calendar.timegm(time.gmtime())
                json.loads(self.send_request(location_url, self.not_moving_data[non_moving_drone_id]))

            time.sleep(1)

    def send_request(self, url, post_data):
        request = Request(url, urlencode(post_data).encode())
        return urlopen(request).read().decode()

    def get_random_string(self):
        char_set = string.ascii_letters + string.digits
        return "".join(random.choice(char_set) for _ in range(5))


if __name__ == "__main__":
    simulator = DroneSwarmSimulation()
    simulator.init_swarm()
    simulator.start_swarm_simulation()
