**Drone Monitor**

****Overview****
This app allows following functionality to monitor drone location data and speed.
And lets user know if drone is currently moving or not and whether drone is sending
location data or not.

Few specific feature of this app are  
1. Allowing owner based viewing of drone data. 
   A owner can view data for only those drones which are assigned to owner.
   Except for root("root101") owner.

2. The owner can access dashboard and provide owner_id and api_key to 
   access drone data
   
3. Not moving drones are highlighted with red color
4. Not updating drones are highlighted with yellow color   

****Tech Stack****
Back-End of this app is coded in python3 with flask and flask-socketio  
framework, gunicorn server with eventlet and geopy library for precise great circle
distance computation

Front-End is mainly bootstrap and jquery.

****Building and Running from docker****
A Dockerfile is provided in docker directory, to build and run server image.
User can also do following from project's root directory

 1. To build docker image  
    chmod +x build_docker_image.sh  
    ./build_docker_image.sh  
    
    (Please ignore "invalid coordinate warning" during build process  ,  
     As testcase running is also integrated in build process,  
     the warning(expected result) is just an outcome of specific unit test)
 
 
 2. To run docker image  
    chmod +x run_docker_image.sh  
    ./run docker_image.sh     
    
and then dashboard can be accessed at **http://0.0.0.0:5000**

****Using dashboard****
(Please refer docs/API_Document for specific API reques details)

Dashboard can be accessed at http://0.0.0.0:5000
User is need to provide owner_id and api_key.
User can use root credentials below 
ownerid: root101
api_key: OhE7ORDPD6mvhnsjzQvjQ373q
And then click on subscribe.

But initially there are no drones. User can run simulation.py(more in next section).  
  
  
Or create new drones and owners using postman tool(see docs/API_Document for details)
User can use root credentials to create new drones and then send drone data with drone_id
Or User can create new owner and then create drones for those owner and send drone data.
**root101 along with its api_key will be able to view all drones**

**Running Simulation**
After starting server, user can run simulation.py from project's root directory
Using **python3 simulation.py**. It will print out few ownerIds and api_keys.    
User can use generated credentials or use the root credentials in dashboard.

**simulation.py mimics a drone swarm and send location data to server for 1 minute
and then ask user to continue furthur or stop**.  
User can view the location data in dashboard.  
(Also see **docs/Simulatiom.md**)

**Other Information**
[Brief backend architecure information](https://drive.google.com/file/d/1jg8LfWT2pPwW2GPYKq0Ceqx2cYUxngLq/view?usp=sharing)
**Please refer to docs directory , 
specially docs/API_Document.md for architectural decisions** .



    